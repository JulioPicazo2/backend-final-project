package com.JCPG.ws.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.JCPG.ws.beans._dayx;
import com.JCPG.ws.services.Day_Serv;

@CrossOrigin
@RestController
@RequestMapping("api/v1")
public class Day_Controller {
	
	@Autowired
	Day_Serv _dayService;
	@GetMapping(path =  "/days", produces = "application/json")
	public List<_dayx> gettingDays(){
		// TODO Auto-generated method stub
		
		return _dayService.days();
	}
	
	@GetMapping(path =  "/users", produces = "application/json")
	public List<_dayx> gettingUsers(){
		// TODO Auto-generated method stub
		
		return _dayService.days();
	}
	

}