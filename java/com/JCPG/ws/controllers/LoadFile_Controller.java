package com.JCPG.ws.controllers;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.JCPG.ws.services.LoadFile_Serv;

@CrossOrigin
@RestController
@RequestMapping("api/v1")
public class LoadFile_Controller {

	@Autowired
	private LoadFile_Serv _updateService;
	
    @RequestMapping("/load")
    public String loadFIleExcel() throws ParseException, IOException {
    	_updateService.loading();
    	
    	
        return "0";
    }
	
}
