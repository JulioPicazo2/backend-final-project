package com.JCPG.ws.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.JCPG.ws.beans._dayx;
import com.JCPG.ws.beans._monthx;
import com.JCPG.ws.services.Month_Serv;

@CrossOrigin
@RestController
@RequestMapping("api/v1")
public class Month_Controller {
	
	@Autowired
	private Month_Serv _monthService;
	
	@GetMapping(path =  "/users/{is}/{month}", produces = "application/json")
	public _monthx user_is_month(@PathVariable String is, @PathVariable Long month) {
		// TODO Auto-generated method stub
		
		return _monthService.MonthAndIs(is, month);
	}
	
	@GetMapping(path =  "/periods/{month}", produces = "application/json")
	public _monthx is_month(@PathVariable Long month) {
		// TODO Auto-generated method stub
		
		return _monthService.just_mo(month);
	}

	@GetMapping(path =  "/period/{month}", produces = "application/json")
	public List<_monthx> all_month(@PathVariable Long month){
		// TODO Auto-generated method stub
		
		return _monthService.months(month);
	}
	
}
