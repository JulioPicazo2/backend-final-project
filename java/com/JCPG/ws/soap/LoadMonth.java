package com.JCPG.ws.soap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.JCPG.ws.beans._dayx;
import com.JCPG.ws.beans._monthx;

@Service
public class LoadMonth {
	
    private static final String FILE_NAME = "Registros_Momentum.xls";

	List<_dayx> days = new ArrayList<>();
	List<_monthx> months = new ArrayList<>();
	
	_dayx day2 = new _dayx();
	Date nDate;
	int monthS;
	int monthSN;
	long secN;
	long total;
	int sec2;
	
	public Object[] setMonthInfo() throws ParseException, IOException{
		
		LoadDay load = new LoadDay();
		
		days = load.getAllDays();
		

		Iterator<_dayx> iDays = days.iterator();
		day2 = iDays.next();
		
		while(iDays.hasNext()) 
		{
			_monthx monthA2 = new _monthx();
		    String a;
		    monthA2.setIsstr(day2.getIsstr());
		    
		    nDate = new SimpleDateFormat("yyyy-MM-dd").parse(day2.getDatestr());
		    monthS = nDate.getMonth() + 1 ;
		    monthSN = monthS;
			total = 0;
			a = day2.getIsstr();
			
		    while(monthSN == monthS && a.equals(day2.getIsstr())) 
		    {
		    	total = total + day2.getSeconds();
			    if(!iDays.hasNext()) 
			    {
			    		break;
			    }
			    else 
			    {
			    	day2 = iDays.next();
			    	nDate = new SimpleDateFormat("yyyy-MM-dd").parse(day2.getDatestr());
			    	monthSN = nDate.getMonth() + 1 ;
			    }
		    }
		    monthA2.setImes(monthS);
		    monthA2.setSeconds(total);
		    months.add(monthA2);

		}
		Object[] returns = {days, months};
		return returns;
		
	}
	
	public void SetMonthInfoExcel() throws IOException {
		String[] c = {"Name", "Month", "Month Time","Prueba"};
		if(months.isEmpty()) { }else {}
	
		try {

        HSSFWorkbook sheetFile = new HSSFWorkbook(); 
         
        Sheet sheet = sheetFile.createSheet("Employee Data");
        
        Row row;
        Cell cell;
        
		row = sheet.createRow(0);
		cell = row.createCell(0);
		cell.setCellValue("Name");
		cell = row.createCell(1);
		cell.setCellValue("Month");
		cell = row.createCell(2);
		cell.setCellValue("Month Time");
		
		int i = 1;
		for(_monthx m : months) {
			
			//Para identificar cual donde va cada cosa
	        Row cRow;
	        
			cRow = sheet.createRow(i);
			
			Cell cCell = cRow.createCell(0);
			cCell.setCellValue(m.getIsstr());
			
			cCell = cRow.createCell(1);
			cCell.setCellValue(m.getImes());
			
			cCell = cRow.createCell(2);
			cCell.setCellValue(m.getSeconds());
			
			i++;
			
			}
		
        
        FileOutputStream finalOta = new FileOutputStream(new File("momentum.xls"));
        sheetFile.write(finalOta);
        finalOta.close();
        
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}
}
