package com.JCPG.ws.soap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.JCPG.ws.beans._dayx;


@Service
public class LoadDay {
	
    private static final String FILE_NAME = "Registros_Momentum.xls";
    public static List<_dayx> toSaveDay = new ArrayList<>();
    
    String hoursN = null;
    String isN = null;
    String datesFile;
    String dateNN;
    String hoursFile;
    //Dates
	Date dateN;
	Date hoursNN = null;
	Date hourNow; 
	Date dateNow;
	
    
	public List<_dayx> getAllDays() throws ParseException{
		 try {
			 	//Instances
	            FileInputStream loadFile = new FileInputStream(new File(FILE_NAME));
	            Workbook sheetBook = new HSSFWorkbook(loadFile);
	            Sheet sheetT = sheetBook.getSheetAt(0);
	            Iterator<Row> iteratorDays = sheetT.iterator();
	            
	            
	            iteratorDays.next();
	            
	            Row cRow = iteratorDays.next();
	            while (iteratorDays.hasNext()) {
	            	_dayx newDay = new _dayx();
	                Cell cCell = cRow.getCell(2);
	            	if (cCell.getCellTypeEnum() == CellType.STRING) 
	            	{
	                    isN = cCell.getStringCellValue();
	            	} 
	            	cCell = cRow.getCell(4);
	            
	            	if (cCell.getCellTypeEnum() == CellType.STRING) 
	            	{
	            		dateNow = new SimpleDateFormat("yyyy-MM-dd").parse(cCell.getStringCellValue());
	            		hourNow = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cCell.getStringCellValue());
	            		
	            		DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");  
	            		DateFormat formatHours = new SimpleDateFormat("HH:mm:ss");  
	            		
	            		datesFile = formatDate.format(dateNow);
	            		hoursFile = formatHours.format(hourNow);	
	            		
	            		cRow =  iteratorDays.next();
	            		cCell = cRow.getCell(4);
	            		dateN = new SimpleDateFormat("yyyy-MM-dd").parse(cCell.getStringCellValue());
	            		hoursN = null;

	            		while(dateN.equals(dateNow) && isN.equals(cRow.getCell(2).toString())) 
	            		{
		            		hoursNN = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cCell.getStringCellValue()); 
		                	hoursN = formatHours.format(hoursNN);
		                			                	
		                	if(iteratorDays.hasNext() == false) 
		                	{
		                			break;
		                	}
		                	
		                	cRow = iteratorDays.next();
		                	cCell = cRow.getCell(4);
		                	dateN = new SimpleDateFormat("yyyy-MM-dd").parse(cCell.getStringCellValue());	
	            		}
	            		
	            		if(!hoursFile.equals(hoursN) && hoursN != null)
	            		{
	                		newDay.setDatestr(datesFile);
	                		newDay.setCout(hoursN);
	                		newDay.setCin(hoursFile);
	                		newDay.setIsstr(isN);                		
	                		hourNow = new SimpleDateFormat("hh:mm:ss").parse(hoursFile);
	                		hoursNN = new SimpleDateFormat("hh:mm:ss").parse(hoursN);
	                		if(hoursNN.getHours()== 0) hoursNN.setHours(12);
	                		if(hourNow.getHours()== 0) hourNow.setHours(12);
	                		
	                        long i = (hoursNN.getTime() - hourNow.getTime())/1000;
	                    	newDay.setSeconds(i);
	                    		                    	
	                        toSaveDay.add(newDay);
	                	}
	            	}
	            	
	            }
	            return toSaveDay;
	            
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        	return null;
	        } catch (IOException e) {
	            e.printStackTrace();
	        	return null;
	        }
		
	}
}
