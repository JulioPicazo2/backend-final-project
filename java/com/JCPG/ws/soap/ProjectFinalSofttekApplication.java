package com.JCPG.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.JCPG.ws")
@EnableTransactionManagement
@EnableJpaAuditing
@EntityScan("com.JCPG.ws.beans")
@EnableJpaRepositories("com.JCPG.ws.repositories")
public class ProjectFinalSofttekApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectFinalSofttekApplication.class, args);
	}

}
