package com.JCPG.ws.implementations;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.JCPG.ws.beans._dayx;
import com.JCPG.ws.beans._monthx;
import com.JCPG.ws.repositories.Days_repository;
import com.JCPG.ws.repositories.Months_repository;
import com.JCPG.ws.services.LoadFile_Serv;
import com.JCPG.ws.soap.LoadMonth;

@Service
public class LoadFservice_Impl implements LoadFile_Serv{

	@Autowired
	private Days_repository _dayRepository;	
	@Autowired
	private Months_repository _monthRepository;	

	public void callingMethod() {
		
	}
	@Override
	public int loading() throws ParseException, IOException {
		// TODO Auto-generated method stub
		
		LoadMonth info = new LoadMonth();
    	Object[] obj = info.setMonthInfo();
    	List<_dayx> days = (List<_dayx>) obj[0];
    	List<_monthx> months = (List<_monthx>) obj[1];
    	
    	info.SetMonthInfoExcel();
    	
    	
    	_dayRepository.deleteAll();
    	_dayRepository.saveAll(days);
    	_monthRepository.deleteAll();
    	_monthRepository.saveAll(months);
    	
		
		return 0;
	}

}
