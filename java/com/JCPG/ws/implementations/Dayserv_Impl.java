package com.JCPG.ws.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.JCPG.ws.beans._dayx;
import com.JCPG.ws.repositories.Days_repository;
import com.JCPG.ws.services.Day_Serv;

@Service
public class Dayserv_Impl implements Day_Serv {

	@Autowired
	private Days_repository _dayRepository;
	
	@Override
	public List<_dayx> days() {
		// TODO Auto-generated method stub
		
		return _dayRepository.findAll();
	}
	
}
