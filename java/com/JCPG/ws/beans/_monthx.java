package com.JCPG.ws.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class _monthx {
	@Id
	@GeneratedValue
	private long month_id;
	private long imes;
	private long month_seconds;
	private String isstr;
	private long month_hours;
	public long getHours() {
		month_hours = this.month_seconds / 3600;
		return month_hours;
	}
	public void setHours(long month_hours) {
		this.month_hours = month_hours;
	}

	public _monthx() {
		super();
	}
	
	public _monthx(long month_id, long imes, long month_seconds, String isstr, long month_hours) {
		super();
		this.month_id = month_id;
		this.imes = imes;
		this.month_seconds = month_seconds;
		this.isstr = isstr;
		this.month_hours = month_seconds / 3600;
	}
	
	public long getId() {
		return month_id;
	}
	public void setId(long month_id) {
		this.month_id = month_id;
	}
	public long getImes() {
		return imes;
	}
	public void setImes(long imes) {
		this.imes = imes;
	}
	public long getSeconds() {
		return month_seconds;
	}
	public void setSeconds(long month_seconds) {
		this.month_seconds = month_seconds;
	}
	public String getIsstr() {
		return isstr;
	}
	public void setIsstr(String isstr) {
		this.isstr = isstr;
	}
	
	@Override
	public String toString() {
		return "Month [id=" + month_id + ", imes=" + imes + ", seconds=" + month_seconds + ", isstr=" + isstr + "]";
	}
	
	
	
}
