package com.JCPG.ws.beans;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class _dayx {
    @Id
    @GeneratedValue
    private long day_id;
    private String isstr; 
    private String day_datestr;
    private String day_cin;
    private String day_cout;
    private long day_seconds;
    private long day_hours;
       
    public long getHours() {
    	day_hours = this.day_seconds / 3600;
		return day_hours;
	}
	public void setHours(long day_hours) {
		this.day_hours = day_hours;
	}	
	public _dayx() {
        super();
    }
    public _dayx(long day_id, String isstr, String day_datestr, String day_cin, String day_cout, long day_seconds, long day_hours) {
        super();
        this.day_id = day_id;
        this.isstr = isstr;
        this.day_datestr = day_datestr;
        this.day_cin = day_cin;
        this.day_cout = day_cout;
        this.day_seconds = day_seconds;
        this.day_hours = day_seconds / 3600;
    }
    public long getId() {
        return day_id;
    }
    public void setId(long day_id) {
        this.day_id = day_id;
    }
    public String getIsstr() {
        return isstr;
    }
    public void setIsstr(String isstr) {
        this.isstr = isstr;
    }
    public String getDatestr() {
        return day_datestr;
    }
    public void setDatestr(String day_datestr) {
        this.day_datestr = day_datestr;
    }
    public String getCin() {
        return day_cin;
    }
    public void setCin(String day_cin) {
        this.day_cin = day_cin;
    }
    public String getCout() {
        return day_cout;
    }
    public void setCout(String day_cout) {
        this.day_cout = day_cout;
    }
    public long getSeconds() {
        return day_seconds;
    }
    public void setSeconds(long day_seconds) {
        this.day_seconds = day_seconds;
    }
    @Override
    public String toString() {
        return "Day [day_id=" + day_id + ", day_isstr=" + isstr + ", day_datestr=" + day_datestr + ", day_cin=" + day_cin + ", day_cout=" + day_cout + ", day_seconds="
                + day_seconds + ", day_hours=" + day_hours + "]";
    }
    
}