package com.JCPG.ws.services;

import java.io.IOException;
import java.text.ParseException;

public interface LoadFile_Serv {
	
	int loading() throws ParseException, IOException;
}
