package com.JCPG.ws.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.JCPG.ws.beans._dayx;

@Repository("_dayRepository")
@Transactional
public interface Days_repository extends JpaRepository<_dayx, Long> {
	_dayx isstr(String isstr);
}
