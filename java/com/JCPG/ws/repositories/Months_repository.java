package com.JCPG.ws.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.JCPG.ws.beans._monthx;

@Repository
public interface Months_repository extends JpaRepository<_monthx, Long> {
	// TODO Auto-generated method stub

	_monthx IsstrAndImes(String isstr, Long month);
	_monthx Imes(Long imes);
	
	//System.out.println();
}
