package com.JCPG.ws.soap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import com.JCPG.ws.beans._dayx;
import com.JCPG.ws.beans._monthx;
import com.JCPG.ws.repositories.Days_repository;


@SpringBootTest
public class LoadingExcelTest {
	
	@Autowired
	private LoadMonth _load;
    private static final String name = "Registros_Momentum.xls";
	@Autowired
	private Days_repository _dayRepository;
	@Autowired
	private TestRestTemplate restTemplate;
	@LocalServerPort
	private int port;
	
    
    @Test
	public void toFindTheInfo() throws ParseException, IOException {
		//Para que las listas no esten vacias
    	//Setup
    	
    	//Execute
		Object[] ret = _load.setMonthInfo();
		List<_dayx> days = (List<_dayx>) ret[0];
		List<_monthx> meses = (List<_monthx>) ret[1];
		
		//Verify
		assertNotNull(days);
	
		assertNotNull(meses);
	}
	
	@Test 
	public void ifIs() {
		//QUE EL ARCHIVO EXISTA
		//Setup
		//Execute
        File file = new File(name);
      //Verify
        assertTrue(file.exists());
	}
	
	@Test 
	public void checkDay(){
		//Que exista la tabla month
	
		//Setup
		//Execute
		List<_dayx> days = _dayRepository.findAll();
		//Verify
		assertNotNull(days);

	}
	
	@Test
	public void greetingShouldReturnDefaultMessage() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
				String.class)).contains("Hello, World");
	}
	
	
}
