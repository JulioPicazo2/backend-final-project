package com.JCPG.ws.soap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

//@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { ProjectFinalSofttekApplication.class })
public class PruebaIntegracion {
	
    private MockMvc _mok;

    @SuppressWarnings("deprecation")
	@Test
    public void APIUser() throws Exception {
    	_mok.perform(get("api/v1/users").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
    
}
